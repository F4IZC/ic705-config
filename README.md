# IC-705 Programming and memory groups

Configuration and memory group files (like a codeplug) to program ICOM IC-705 with Icom software's.



## Getting started

You can download [CS-705 on Icom website](https://www.icomjapan.com/support/firmware_driver/3708/).

Once you have download the memory group you need, simply go to File > Import > Group... and select the file.

## Contributing
Those files are essentially building on my own needs and usages. If you want to complete them, don't hesitate to share your version. After review completed files will be published here.

## License
This work is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
